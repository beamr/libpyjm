#
# Copyright (c) 2019 Beamr Imaging Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to 
# deal in the Software without restriction, including without limitation the 
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
# sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#

"""
libpyjm

JPEGmini Python binding
This is a Cython wrapper for libjm (JPEGmini) to optimize jpeg images.
See http://jpegmini.com or contact us at support@jpegmini.com for further info
"""

cdef enum: REMOVE_METADATA = 0
cdef enum: SKIP_HIGHLY_COMPRESSED = 0

cimport clibpyjm

cdef enum JM_QUALITY:
    BEST = 0
    HIGH = 1
    MEDIUM = 2

logger = None

def _log(msg):
    if logger:
        logger(msg)

class JMException(Exception):
    pass


cdef clibpyjm.JMHANDLE _jm #handle to jpegmini library
cdef int res = clibpyjm.jm_trans_init(&_jm)
if res != 0:
    _log('JPEGmini library failed to initialize. Error %d' % res)
    raise JMException('Unable to start JPEGmini library')
else:
    _log('JPEGmini library initialized')


def unload():
    global _jm
    if _jm is not NULL:        
        clibpyjm.jm_trans_fini(_jm)
        _jm = NULL
        _log('JPEGmini library unloaded')
    else:
        raise JMException('JPEGmini library already unloaded')



cdef class PyJM:
    """
    """
    cdef clibpyjm.JM_JPEG_HANDLE _img_handle    #handle to source image
    cdef clibpyjm.JMIMGINFO _info               #handle to info structure
    cdef _img_data                              #reference to source data

    def __cinit__(self):
       self._img_handle = NULL
       self._img_data = None

    def __dealloc__(self):
        if self._img_handle is not NULL:
            self.free_image()
        self._img_data = None

    def version(self):
        """
        Get library version info
        """
        return clibpyjm.jm_get_version()

    def set_image(self, src_data):
        """
        Initialize library with image data.
        This method must be called once per image before processing.

        Parameters:
        src_data - a string/bytearray of the source jpeg image
        """
        _log('Allocating new image struct with refernce to source data')
        self._img_data = src_data  #add reference to src_data
        res = clibpyjm.jm_image_new(_jm, self._img_data, len(self._img_data), &self._img_handle)
        if res != 0:
            _log('Error allocating internal image struct. Error: %d' % res)
            raise JMException('Error allocating internal image struct. Error: %d' % res)

    def free_image(self):
        """
        Release the image_data from the library.
        This method must be called once per image after processing.
        """

        if self._img_handle is not NULL:
            _log('Releasing internal image struct and reference to source data')
            clibpyjm.jm_image_free(self._img_handle)
            self._img_handle = NULL
            self._img_data = None
        else:
            _log('No image loaded')


    def get_info(self):
        """
        Get information about the current image
        returns a dictionary with image attributes:
        size - image size in bytes
        width - image width in pixels
        height - image height in pixels
        """
        cdef int res
        if self._img_handle is not NULL:
            with nogil:
                res = clibpyjm.jm_img_info(self._img_handle, &self._info)
            if res != 0:
                _log('Error getting image info. Error %d' % res)
                raise JMException('Error reading image info. Error %d' % res)
 
            return {
                'size': len(self._img_data),
                'width': self._info.width,
                'height': self._info.height,
            }
        else:
            raise JMException('No image loaded')


    def optimize(self, scale=None, width=None, height=None):
        """
        Optimize the current image
        This method may be called multiple times, with different parameters 
        
        Parameters:
        scale (optional) - scale factor, float between 0 and 1
        width (optional) - target width, in pixels (must be <= input widht)
        height (optonal) - target height, in pixels (must be <= input height)
        
        * If no parameters are passed, the size is maintained (scale=1.0)
        * You cannot specify both scale and (width or height)
        * If only width or height are provided, the aspect ratio is maintained

        Returns a dictionary with the following fields:
        width - width of output image (in pixels)
        height - height of output image (in pixels)
        scale - scale factor
        size - size of output image (in bytes)
        data - jpeg data of output image.
        """
        cdef clibpyjm.JMIMGBUF* _mini_handle   #handle to mini image
        cdef float c_scale
        cdef int c_width, c_height
        cdef int res = -1
        size = None
        data = None

        assert not (scale and (width or height))

        if not (scale or width or height):
            scale = 1.0

        if self._img_handle is not NULL:
            if scale:
                c_scale = scale
                with nogil:
                    res = clibpyjm.jm_image_recompress(self._img_handle, c_scale, REMOVE_METADATA, SKIP_HIGHLY_COMPRESSED, BEST, &_mini_handle)
                width = round(scale * self._info.width)
                height = round(scale *self._info.height)
            elif width and height:
                c_width, c_height = width, height                
                with nogil:
                    res = clibpyjm.jm_image_recompress_dim(self._img_handle, c_width, c_height, REMOVE_METADATA, SKIP_HIGHLY_COMPRESSED, BEST, &_mini_handle)
            else:     
                tmp = self.get_info() # called to init _info, if not already set
                if width and not height:
                    height = round(1.0 * self._info.height / self._info.width * width)
                if height and not width:
                    width = round(1.0 * self._info.width / self._info.height * height)
                c_width, c_height = width, height
                with nogil:
                    res = clibpyjm.jm_image_recompress_dim(self._img_handle, c_width, c_height, REMOVE_METADATA, SKIP_HIGHLY_COMPRESSED, BEST, &_mini_handle)
                scale = 1.0 * width / self._info.width
            if res != 0: 
                _log('Error %d optimizing image' % res)
                raise JMException('Error optimizing image. Error: %d' % res)

            try:
                size = _mini_handle.size
                data = _mini_handle.img[:_mini_handle.size]
            finally:
                with nogil:
                    clibpyjm.jm_free_img_buf(_mini_handle)

            return { 
                'width': width, 
                'height': height, 
                'scale': scale, 
                'size': size, 
                'data': data,
            }
        else:
            raise JMException('No image loaded')

    

