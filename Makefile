all: clean libpyjm.so

libpyjm.so:
	CFLAGS="-I/usr/include/jpegmini -I/usr/local/include/jpegmini -I/usr/include -I/usr/local/include" LDFLAGS="-L/usr/local/lib -L/usr/lib" python setup.py build_ext --inplace

clean:
	rm -rf libpyjm.so
	rm -rf libpyjm.c
	rm -rf build/
	rm -rf cython_debug
	rm -rf tests/*mini*

test:
	./test.py

install: libpyjm.so
	cp libpyjm.so /usr/lib
	ldconfig
