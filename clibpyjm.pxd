#
# Copyright (c) 2019 Beamr Imaging Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to 
# deal in the Software without restriction, including without limitation the 
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
# sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#

cdef extern from "JPEGminiAPI.h":
    ctypedef enum JM_RES_CODE:
        pass

    ctypedef void* JMHANDLE
    ctypedef void* JM_JPEG_HANDLE

    ctypedef struct JMIMGBUF:
        unsigned char* img
        unsigned long size

    ctypedef struct JMIMGINFO:
        float size_MP
        int is_jpeg_mini_trs
        int width
        int height

    ctypedef enum JM_QUALITY:
        pass

    JM_RES_CODE jm_trans_init(JMHANDLE* libpyjm) nogil

    JM_RES_CODE jm_image_new(JMHANDLE libpyjm,
        unsigned char* jpeg_buf,
        unsigned long jpeg_size,
        JM_JPEG_HANDLE* jm_img) nogil

    JM_RES_CODE jm_img_info(JM_JPEG_HANDLE jm_img, JMIMGINFO* info) nogil

    JM_RES_CODE jm_image_recompress(JM_JPEG_HANDLE jm_img,
        float resizing,
        int fremove_metadata,
        int fskip_highly_compressed,
        JM_QUALITY quality,
        JMIMGBUF** img_buf) nogil

    JM_RES_CODE jm_image_recompress_dim(JM_JPEG_HANDLE jm_img,
        long  width_dst,
        long  height_dst,
        int fremove_metadata,
        int fskip_highly_compressed,
        JM_QUALITY quality,
        JMIMGBUF** img_buf) nogil

    JM_RES_CODE jm_image_resize(JM_JPEG_HANDLE jm_img,
        float resizing,
        JMIMGBUF** img_buf) nogil

    void jm_image_free(JM_JPEG_HANDLE jm_img) nogil

    void jm_free_img_buf(JMIMGBUF* img_buf) nogil

    void jm_trans_fini(JMHANDLE libpyjm) nogil

    char* jm_get_version() nogil

