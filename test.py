#!/usr/bin/env python
#
# Copyright (c) 2013 ICVT 
#
# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the "Software"), to 
# deal in the Software without restriction, including without limitation the 
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
# sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in 
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#



import os.path
import libpyjm

# Initialize logging function
def my_log(msg):
    print msg
libpyjm.logger = my_log

# Initialize JPEGmini library wrapper
jm = libpyjm.PyJM()

def test_optimizeing_image(input_filename, output_filename):
    #load image into library
    src = open(input_filename, 'r')
    jm.set_image(src.read())
    
    try:
        # optimize without resize
        res = jm.optimize()
        with open(output_filename, 'w') as dst:
            dst.write(res['data'])

        # optimize with various resize options
        res = jm.optimize(scale=0.5)
        with open(output_filename + '.scale', 'w') as dst:
            dst.write(res['data'])
  
        res = jm.optimize(width=100)
        with open(output_filename + '.width', 'w') as dst:
            dst.write(res['data'])
    
        res = jm.optimize(height=100)
        with open(output_filename + '.height', 'w') as dst:
            dst.write(res['data'])

        res = jm.optimize(height=100, width=100)
        with open(output_filename + '.wh', 'w') as dst:
            dst.write(res['data'])
    finally:
        jm.free_image() 


if __name__ == '__main__':
    test_optimizeing_image(r'tests/rolands.lakis.jpg', r'tests/rolands.lakis.jpg.mini')
    assert os.path.getsize(r'tests/rolands.lakis.jpg.mini') > 1200000
    assert os.path.getsize(r'tests/rolands.lakis.jpg.mini.scale') > 35000
    assert os.path.getsize(r'tests/rolands.lakis.jpg.mini.width') > 8000
    assert os.path.getsize(r'tests/rolands.lakis.jpg.mini.height') > 8000
    assert os.path.getsize(r'tests/rolands.lakis.jpg.mini.wh') > 8000
    print 'All is good!'
